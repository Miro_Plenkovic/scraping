﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Text;

namespace Migrations
{
    [Migration(20201130121800)]
    public class Initial : Migration
    {
        public override void Up()
        {
            Create.Table("scraping_data").InSchema("igscraping")
                .WithColumn("id").AsInt32().PrimaryKey().Identity()
                .WithColumn("username").AsString()
                .WithColumn("followers").AsInt64()
                .WithColumn("created_datetime").AsDateTime();
            Create.Table("likes_history").InSchema("igscraping")
                .WithColumn("id").AsInt32().PrimaryKey().Identity()
                .WithColumn("username").AsString()
                .WithColumn("current_likes").AsString()
                .WithColumn("created_datetime").AsDateTime();
            Create.Table("likes_current").InSchema("igscraping")
                .WithColumn("id").AsInt32().PrimaryKey().Identity()
                .WithColumn("username").AsString()
                .WithColumn("likes_count").AsString();
        }

        public override void Down()
        {
            Delete.Table("scraping_data").InSchema("igscraping");
            Delete.Table("likes_history").InSchema("igscraping");
            Delete.Table("likes_current").InSchema("igscraping");
        }
    }
}
