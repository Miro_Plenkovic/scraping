﻿using Microsoft.EntityFrameworkCore;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace ScraperTest
{
    public class DatabaseContext : Microsoft.EntityFrameworkCore.DbContext
    {
        public Microsoft.EntityFrameworkCore.DbSet<ScrapingData> ScrapingData { get; set; }
        public Microsoft.EntityFrameworkCore.DbSet<LikesCurrent> LikesCurrent { get; set; }
        public Microsoft.EntityFrameworkCore.DbSet<LikesHistory> LikesHistory { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql("server=localhost;port=5432;database=igscraping;username=postgres;password=postgres;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //Configure default schema
            modelBuilder.HasDefaultSchema("igscraping");
        }
    }

    [Table("scraping_data")]
    public class ScrapingData
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("id")]
        public int id { get; set; }
        [Column("username")]
        public string Username { get; set; }
        [Column("followers")]
        public int FollowerCount { get; set; }
        [Column("created_datetime")]
        public DateTimeOffset CreatedDatetime { get; set; }
    }

    [Table("likes_history")]
    public class LikesHistory
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("id")]
        public int id { get; set; }
        [Column("username")]
        public string Username { get; set; }
        [Column("current_likes")]
        public string CurrentLikes { get; set; }
        [Column("created_datetime")]
        public DateTimeOffset CreatedDatetime { get; set; }
    }

    [Table("likes_current")]
    public class LikesCurrent
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("id")]
        public int id { get; set; }
        [Column("username")]
        public string Username { get; set; }
        [Column("likes_count")]
        public string LikesCount { get; set; }
    }
}
