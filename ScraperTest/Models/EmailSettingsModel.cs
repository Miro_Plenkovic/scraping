﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ScraperTest.Models
{
    public class EmailSettingsModel
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
