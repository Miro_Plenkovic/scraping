﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Toolkit.Uwp.Notifications;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using ScraperTest.Models;
using ScraperTest.Services.Hosted;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

namespace ScraperTest
{
    class Program
    {
        static IConfigurationRoot Configuration { get; set; }
        static void Main(string[] args)
        {
            var host = new HostBuilder()
                .UseConsoleLifetime()
                .ConfigureHostConfiguration((configuration) => { Configuration = configuration.AddJsonFile("appsettings.json").Build(); })
                .ConfigureServices((hostContext, services) =>
                {
                    services.Configure<EmailSettingsModel>(Configuration.GetSection("emailSettings"));
                    //services.AddHostedService<LikesScrapingService>();
                    services.AddHostedService<IGScraperService>();
                }).Build();
            host.Run();
        }
    }
}
