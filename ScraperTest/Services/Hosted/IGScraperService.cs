﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Microsoft.Toolkit.Uwp.Notifications;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using ScraperTest.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Windows.UI.Notifications;

namespace ScraperTest.Services.Hosted
{
    public class IGScraperService : IHostedService
    {
        private readonly EmailSettingsModel _settings;
        public IGScraperService(IOptionsSnapshot<EmailSettingsModel> settings)
        {
            _settings = settings.Value;
        }
        private static Timer _timer;
        private static int index = 0;
        private static string[] accs = new string[] { "cauzifer", "wanderlustluca", "nymphahri", "ehri", "littlejem"};
        private static Dictionary<string, int> thresholds = new Dictionary<string, int>();
        public async Task StartAsync(CancellationToken cancellationToken)
        {
            thresholds[accs[0]] = 110000;
            thresholds[accs[1]] = 200000;
            thresholds[accs[2]] = 230000;
            thresholds[accs[3]] = 130000;
            thresholds[accs[4]] = 210000;
            var chromeOptions = new ChromeOptions();
            //chromeOptions.AddArguments("headless");
            chromeOptions.AddArguments("@user-data-dir=C:/ChromeProfile/Profile1");
            using (var driver = new ChromeDriver((Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)), chromeOptions))
            {
                try
                {
                    driver.Navigate().GoToUrl("https://blastup.com/stats");
                    WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(20));
                    IWebElement element = wait.Until(ExpectedConditions.ElementToBeClickable(By.Id("follower-count-input")));
                    await Task.Delay(5000);
                    _timer = new Timer(async o => await CheckAmount(o, driver), null, (int)TimeSpan.FromSeconds(5).TotalMilliseconds, Timeout.Infinite);
                    Console.ReadLine();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }

        public async Task StopAsync(CancellationToken cancellationToken)
        {
        }

        public async Task CheckAmount(object state, ChromeDriver driver)
        {
            try
            {
                using (DatabaseContext dbContext = new DatabaseContext())
                {
                    string username = accs[(index++) % accs.Length];
                    int lastValue = dbContext.ScrapingData.Where(x => x.Username == username).OrderByDescending(x => x.CreatedDatetime).FirstOrDefault()?.FollowerCount ?? 0;
                    driver.FindElement(By.Id("follower-count-input")).SendKeys(username);
                    driver.FindElement(By.XPath("//form[contains(@id, 'follower-count')]//button[contains(@class, 'button blue')]")).Click();
                    bool beepCheckOne = false;
                    bool beepCheckTwo = false;
                    if (lastValue < thresholds[username] && lastValue != 0) beepCheckOne = true;
                    WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
                    IWebElement element = wait.Until(ExpectedConditions.ElementToBeClickable(By.ClassName("odometer-value")));
                    await Task.Delay(1000);
                    int followers = int.Parse(string.Concat(driver.FindElements(By.ClassName("odometer-value")).Select(x =>
                    {
                        try
                        {
                            if (x.Text is string)
                            {
                                return x.Text;
                            }
                            else
                            {
                                return "";
                            }
                        }
                        catch (StaleElementReferenceException sere)
                        {
                            return "";
                        }
                    })));
                    if ((lastValue == 0) || (Math.Abs(followers - lastValue) < lastValue * 0.9m))
                        lastValue = followers;
                    Console.WriteLine(string.Format("{0} : {1}/{2}({3:F2}%) followers", username, lastValue.ToString(), thresholds[username].ToString(), (decimal)100 * lastValue / thresholds[username]));
                    ScrapingData data = new ScrapingData();
                    data.CreatedDatetime = DateTimeOffset.UtcNow;
                    data.FollowerCount = lastValue;
                    data.Username = username;
                    dbContext.ScrapingData.Add(data);
                    await dbContext.SaveChangesAsync();
                    if (lastValue >= thresholds[username]) beepCheckTwo = true;
                    if (beepCheckOne && beepCheckTwo)
                    {
                        MailMessage mail = new MailMessage();
                        SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");

                        mail.From = new MailAddress(_settings.Email);
                        mail.To.Add(_settings.Email);
                        mail.Subject = "Notification";
                        mail.Body = $"{username} has reached {thresholds[username]} followers!";

                        SmtpServer.Port = 587;
                        SmtpServer.DeliveryMethod = SmtpDeliveryMethod.Network;
                        SmtpServer.UseDefaultCredentials = false;
                        SmtpServer.Credentials = new System.Net.NetworkCredential(_settings.Email, _settings.Password);
                        SmtpServer.EnableSsl = true;

                        SmtpServer.Send(mail);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                _timer.Change((int)TimeSpan.FromSeconds(1).TotalMilliseconds, Timeout.Infinite);
            }
        }
    }
}
