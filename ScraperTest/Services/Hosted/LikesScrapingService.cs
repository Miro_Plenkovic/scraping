﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Hosting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Edge;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Interactions.Internal;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

namespace ScraperTest.Services.Hosted
{
    public class LikesScrapingService : IHostedService
    {
        private static Timer _timer;
        private static Dictionary<string, int> lastValues = new Dictionary<string, int>();
        private static int index = 0;
        private static string[] accs = new string[] { "cauzifer", "wanderlustluca", "nymphahri", "ehri", "littlejem" };
        private static decimal[] ranges = new decimal[] { 1.1m, 1.11m, 1.12m, 1.13m, 1.14m, 1.15m, 1.16m, 1.17m, 1.18m, 1.19m, 1.2m, 1.21m, 1.22m, 1.23m, 1.24m, 1.25m, 1.275m, 1.3m, 1.325m, 1.35m, 1.4m, 1.45m, 1.5m, 200m };
        private static Dictionary<string, int> thresholds = new Dictionary<string, int>();
        public async Task StartAsync(CancellationToken cancellationToken)
        {
            var chromeOptions = new ChromeOptions();
            //chromeOptions.AddArguments("headless");
            chromeOptions.AddArguments("@user-data-dir=C:/ChromeProfile/Profile1");
            using (var driver = new ChromeDriver((Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)), chromeOptions))
            {
                try
                {
                    _timer = new Timer(async o => await CheckAmount(o, driver), null, (int)TimeSpan.FromSeconds(5).TotalMilliseconds, Timeout.Infinite);
                    Console.ReadLine();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }

        public async Task StopAsync(CancellationToken cancellationToken)
        {
        }

        static async Task CheckAmount(object state, ChromeDriver driver)
        {
            try
            {
                using (DatabaseContext dbContext = new DatabaseContext())
                {
                    string username = accs[(index++) % accs.Length];
                    driver.Navigate().GoToUrl($"https://www.instagram.com/{username}");

                    try
                    {
                        driver.FindElement(By.XPath("//button[contains(@class, 'aOOlW  bIiDR  ')]")).Click();
                    }
                    catch (Exception ex) { }
                    await Task.Delay(1000);
                    try
                    {
                        driver.FindElement(By.XPath("//button[contains(@class, 'dCJp8 afkep xqRnw')]")).Click();
                    }
                    catch (Exception ex) { }
                    await Task.Delay(1000);

                    try
                    {
                        driver.FindElement(By.XPath("//div[contains(@class, '_7UhW9   xLCgt       qyrsm         h_zdq  uL8Hv     l4b0S    ')]")).Click();
                    }
                    catch (Exception ex) { }
                    await Task.Delay(1000);

                    driver.ExecuteScript("window.scrollBy(0,200)");
                    await Task.Delay(1000);
                    List<IWebElement> list = driver.FindElementsByXPath("//article//div//div//div//div//a").ToList();
                    List<int> likesList = new List<int>();
                    foreach (IWebElement el in list.Take(24))
                    {
                        Actions actionProvider = new Actions(driver);
                        actionProvider.MoveToElement(el).Perform();
                        await Task.Delay(100);
                        List<IWebElement> likes = driver.FindElementsByXPath("//ul[contains(@class, 'Ln-UN')]//li//span").ToList();
                        if (likes.Any()) likesList.Add(int.Parse(likes.First().Text.Replace(",", "").Replace("k", (likes.First().Text.Contains('.')) ? "00" : "000").Replace(".", "")));
                        else likesList.Add(0);
                        await Task.Delay(100);
                    }

                    if (likesList.Count == 24)
                    {
                        likesList.Reverse();
                        LikesHistory neueHist = new LikesHistory { Username = username, CurrentLikes = string.Join(',', likesList), CreatedDatetime = DateTimeOffset.UtcNow };
                        dbContext.LikesHistory.Add(neueHist);
                        if (dbContext.LikesCurrent.Any(x => x.Username == username))
                        {
                            LikesCurrent curr = await dbContext.LikesCurrent.SingleAsync(x => x.Username == username);
                            List<string> splitLikesCount = curr.LikesCount.Split(',').ToList();
                            List<int> parsedLikesCount = splitLikesCount.Where(x => !x.Contains('*')).Select(x => int.Parse(x)).ToList();
                            bool somethingsWrong = false;
                            for (int i = 0; i < 24; i++)
                            {
                                if (likesList[i] != 0 && parsedLikesCount[parsedLikesCount.Count - 24 + i] != 0)
                                {
                                    decimal expression = (decimal)likesList[i] / parsedLikesCount[parsedLikesCount.Count - 24 + i];
                                    if (((expression >= 1) ? expression : 1 / expression) > ranges[i]) somethingsWrong = true;
                                }
                            }
                            if (somethingsWrong)
                            {
                                curr.LikesCount += ",300";
                                CheckForDeletion(curr, likesList);
                            }
                            splitLikesCount = curr.LikesCount.Split(',').ToList();
                            if (somethingsWrong && splitLikesCount.Last().Contains("*")) splitLikesCount.RemoveAt(splitLikesCount.Count - 1);
                            int starCount = 0;
                            for (int i = 0; i < 24; i++)
                            {
                                if (splitLikesCount[splitLikesCount.Count - 24 - splitLikesCount.Where(x => x.Contains('*')).Count() + i + starCount].Contains("*")) starCount++;
                                splitLikesCount[splitLikesCount.Count - 24 - splitLikesCount.Where(x => x.Contains('*')).Count() + i + starCount] = likesList[i].ToString();
                            }
                            curr.LikesCount = string.Join(',', splitLikesCount);
                        }
                        else
                        {
                            LikesCurrent neue = new LikesCurrent { Username = username, LikesCount = string.Join(',', likesList) };
                            dbContext.LikesCurrent.Add(neue);
                        }
                        await dbContext.SaveChangesAsync();
                    }
                    await Task.Delay(1000);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                _timer.Change((int)TimeSpan.FromSeconds(1).TotalMilliseconds, Timeout.Infinite);
            }
        }

        private static bool CheckForDeletion(LikesCurrent curr, List<int> likesList)
        {
            List<string> splitLikesCount = curr.LikesCount.Split(',').ToList();
            List<int> parsedLikesCount = splitLikesCount.Where(x => !x.Contains('*')).Select(x => int.Parse(x)).ToList();
            bool somethingsWrong = false;
            for (int i = 0; i < 24; i++)
            {
                if (likesList[i] != 0 && parsedLikesCount[parsedLikesCount.Count - 24 + i] != 0)
                {
                    decimal expression = (decimal)likesList[i] / parsedLikesCount[parsedLikesCount.Count - 24 + i];
                    if (((expression >= 1) ? expression : 1 / expression) > ranges[i]) somethingsWrong = true;
                }
            }
            if (!somethingsWrong) return true;
            for (int j = 23; j >= 0; j--)
            {
                splitLikesCount = curr.LikesCount.Split(',').ToList();
                if ((splitLikesCount.Where(x => !x.Contains('*')).ToList().Count - 24 - 1) < 0 || curr.LikesCount.Split(',').ToList().Count(x=>x.Contains("*")) > 2) return false;
                int counter = 0;
                int ind = 0;
                foreach(string split in splitLikesCount)
                {
                    if (!split.Contains("*"))
                    {
                        counter++;
                    }
                    if (counter <= splitLikesCount.Where(x => !x.Contains('*')).ToList().Count - 24 + j) ind++;
                }
                splitLikesCount[ind] = splitLikesCount[ind] + "*"; 
                curr.LikesCount = string.Join(',', splitLikesCount);
                parsedLikesCount = splitLikesCount.Where(x => !x.Contains('*')).Select(x => int.Parse(x)).ToList();
                somethingsWrong = false;
                for (int i = 0; i <= j; i++)
                {
                    if (likesList[i] != 0 && parsedLikesCount[parsedLikesCount.Count - 24 + i] != 0)
                    {
                        decimal expression = (decimal)likesList[i] / parsedLikesCount[parsedLikesCount.Count - 24 + i];
                        if (((expression >= 1) ? expression : 1 / expression) > ranges[i]) somethingsWrong = true;
                    }
                }
                if (somethingsWrong && !CheckForDeletion(curr, likesList))
                {
                    splitLikesCount[splitLikesCount.Count - 24 + j] = splitLikesCount[splitLikesCount.Count - 24 + j].Replace("*", "");
                    curr.LikesCount = string.Join(',', splitLikesCount);
                }
                else if (!somethingsWrong) return true;
            }
            return true;
        }
    }
}
